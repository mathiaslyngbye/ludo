#ifndef PLAYER_MULTI_AGENT_H
#define PLAYER_MULTI_AGENT_H

#include "positions_and_dice.h"
#include "iplayer.h"
#include "move_logic.h"
#include "player_fast.h"
#include "player_random.h"

class player_multi_agent : public iplayer
{
private:
    move_logic logic;
    player_random default_player;
    std::vector<iplayer*> agents;

public:
    player_multi_agent() 
    {
        agents = {&default_player};
    }

    player_multi_agent(std::vector<iplayer*> _agents)
    {
        agents = _agents;
    }

private:
    int  make_decision()
    {
        int piece = -1;
        logic.set(dice, position);

        // Create pos and dice object to provide agents
        positions_and_dice pos_and_dice;
        for(int i = 0; i < 16; i++)
            pos_and_dice.position[i] = position[i];
        pos_and_dice.dice = dice;

        // Get player choices
        int votes[4] = {0, 0, 0, 0};
        for(size_t i = 0; i < agents.size(); i++)
        {
            int choice = agents[i]->make_decision(pos_and_dice);
            if(choice != -1)
                votes[choice]++;
        }

        // Choose most picked piece
        for(int i = 0; i < 4; i++)
        {
            if(votes[i] > piece)
                piece = i;
        }

        return piece;
    }
};

#endif // PLAYER_MULTI_AGENT_H
