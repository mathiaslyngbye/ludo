clear; close; clc 

data01 = readtable("../logs/size100_iterations1000_mutation0.000100_generation500.csv");
data02 = readtable("../logs/size100_iterations1000_mutation0.001000_generation500.csv");
data03 = readtable("../logs/size100_iterations1000_mutation0.010000_generation500.csv");
data04 = readtable("../logs/size100_iterations1000_mutation0.100000_generation500.csv");
data05 = readtable("../logs/size100_iterations1000_mutation0.500000_generation500.csv");

x01 = data01.generation;
y01 = data01.winrate;

x02 = data02.generation;
y02 = data02.winrate;

x03 = data03.generation;
y03 = data03.winrate;

x04 = data04.generation;
y04 = data04.winrate;

x05 = data05.generation;
y05 = data05.winrate;


xi01 = linspace(min(x01), max(x01), 100);
yi01 = interp1(x01, y01, xi01, 'spline', 'extrap');

xi02 = linspace(min(x02), max(x02), 100);
yi02 = interp1(x02, y02, xi02, 'spline', 'extrap');

xi03 = linspace(min(x03), max(x03), 100);
yi03 = interp1(x03, y03, xi03, 'spline', 'extrap');

xi04 = linspace(min(x04), max(x04), 100);
yi04 = interp1(x04, y04, xi04, 'spline', 'extrap');

xi05 = linspace(min(x05), max(x05), 100);
yi05 = interp1(x05, y05, xi05, 'spline', 'extrap');



figure(1)
hold on
plot(xi01, yi01)
plot(xi02, yi02)
plot(xi03, yi03)
plot(xi04, yi04)
%plot(xi05, yi05)

hold off
grid
xlabel('Generation') 
ylabel('Win rate (%)') 

set(gcf,'Position',[100 100 700 300])
