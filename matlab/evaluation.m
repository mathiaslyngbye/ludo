clear; close; clc 

data = readtable("../logs/output.csv");

scatter(data.iterations, data.winrate)

h1 = line([0 1000],[25 25]);
set(h1,'Color','r','LineWidth',2)
xlabel('Iterations') 
ylabel('Win rate (%)') 
set(gcf,'Position',[100 100 700 300])