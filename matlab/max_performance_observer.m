clear; close; clc 

data01 = readtable("../logs/size50_iterations1000_mutation0.200000_stddev1.000000_generation1500.csv");

x01 = data01.generation;
y01 = data01.winrate;

xi01 = linspace(min(x01), max(x01), 100);
yi01 = interp1(x01, y01, xi01, 'spline', 'extrap');

figure(1)
hold on
plot(xi01, yi01)

hold off
grid
xlabel('Generation') 
ylabel('Win rate (%)') 

set(gcf,'Position',[100 100 700 300])