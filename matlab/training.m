clear; close; clc 

data25 = readtable("../logs/size25_iterations1000_mutation0.200000_generation500.csv");
data50 = readtable("../logs/size50_iterations1000_mutation0.200000_generation500.csv");
data75 = readtable("../logs/size75_iterations1000_mutation0.200000_generation500.csv");
data100 = readtable("../logs/size100_iterations1000_mutation0.200000_generation500.csv");

x25 = data25.generation;
y25 = data25.winrate;

x50 = data50.generation;
y50 = data50.winrate;

x75 = data75.generation;
y75 = data75.winrate;

x100 = data100.generation;
y100 = data100.winrate;


xi25 = linspace(min(x25), max(x25), 100);
yi25 = interp1(x25, y25, xi25, 'spline', 'extrap');

xi50 = linspace(min(x50), max(x50), 100);
yi50 = interp1(x50, y50, xi50, 'spline', 'extrap');

xi75 = linspace(min(x75), max(x75), 100);
yi75 = interp1(x75, y75, xi75, 'spline', 'extrap');

xi100 = linspace(min(x100), max(x100), 100);
yi100 = interp1(x100, y100, xi100, 'spline', 'extrap');


figure(1)
hold on
plot(xi25, yi25)
plot(xi50, yi50)
%plot(xi75, yi75)
%plot(xi100, yi100)
hold off
grid
xlabel('Generation') 
ylabel('Win rate (%)') 

set(gcf,'Position',[100 100 700 300])
